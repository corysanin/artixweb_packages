/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

use alpm::{Alpm, AlpmList, Dep, Package, Pkg, Ver};

use crate::error::Error;

/// Retrieves the given `package_name` dependencies
///
/// # Errors
///
/// If the package can not be found a [`Error::PackageNotFound`] error is returned
pub fn get_package_dependencies<'a>(
    alpm: &'a Alpm,
    package_name: &'a str,
) -> Result<AlpmList<'a, &'a Dep>, Error<'a>> {
    for db in alpm.syncdbs() {
        let db_pkgs = db.search([package_name].iter());
        if let Ok(pkgs) = db_pkgs {
            for pkg in pkgs {
                if pkg.name() == package_name {
                    return Ok(pkg.depends());
                }
            }
        }
    }

    Err(Error::PackageNotFound(package_name))
}

/// Retrieves the given `package_name` optional dependencies
///
/// # Errors
///
/// If the package can not be found a [`Error::PackageNotFound`] error is returned
pub fn get_package_optional_dependencies<'a>(
    alpm: &'a Alpm,
    package_name: &'a str,
) -> Result<AlpmList<'a, &'a Dep>, Error<'a>> {
    for db in alpm.syncdbs() {
        let db_pkgs = db.search([package_name].iter());
        if let Ok(pkgs) = db_pkgs {
            for pkg in pkgs {
                if pkg.name() == package_name {
                    return Ok(pkg.optdepends());
                }
            }
        }
    }

    Err(Error::PackageNotFound(package_name))
}

/// Retrieves the given `package_name` version
///
/// # Errors
///
/// if the package can not be found a [`Error::PackageNotFound`] error is returned
pub fn get_package_version<'a>(
    alpm: &'a Alpm,
    package_name: &'a str,
) -> Result<&'a Ver, Error<'a>> {
    for db in alpm.syncdbs() {
        let db_pkgs = db.search([package_name].iter());
        if let Ok(pkgs) = db_pkgs {
            for pkg in pkgs {
                if pkg.name() == package_name {
                    return Ok(pkg.version());
                }
            }
        }
    }

    Err(Error::PackageNotFound(package_name))
}

/// Retrieves the given `package_name` as a full [`alpm::Package`]
///
/// # Errors
///
/// If the package can not be found a [`Error::PackageNotFound`] error is returned
#[allow(clippy::module_name_repetitions)]
pub fn get_package<'a>(alpm: &'a Alpm, package_name: &'a str) -> Result<&'a Pkg, Error<'a>> {
    for db in alpm.syncdbs() {
        let db_pkgs = db.search([package_name].iter());
        if let Ok(pkgs) = db_pkgs {
            if pkgs.len() == 1 {
                if let Some(pkg) = pkgs.first() {
                    return Ok(pkg);
                }
            }

            for pkg in pkgs {
                if pkg.name() == package_name {
                    return Ok(pkg);
                }
            }
        }
    }

    Err(Error::PackageNotFound(package_name))
}

/// Retrieve which packages provide the given requirement
pub fn retrieve_providers<'a>(
    arch: &str,
    alpm: &'a Alpm,
    requirement: &'a str,
) -> Vec<&'a Package> {
    let mut pkgs: Vec<&'a Package> = Vec::new();
    for db in alpm.syncdbs() {
        if arch.to_lowercase() != "any" && db.name() == "lib32" {
            continue;
        }

        for pkg in db.pkgs() {
            let mut a: Vec<&'a Dep> = Vec::new();
            a.extend(pkg.provides().iter().filter(|d| d.name() == requirement));
            if !a.is_empty() {
                pkgs.push(pkg);
            }
        }
    }
    pkgs
}

#[cfg(test)]
mod test {
    use alpm_utils::alpm_with_conf;
    use pacmanconf::Config;

    use super::*;

    #[test]
    fn test_get_package_dependencies() {
        let db_location = format!("{}/tests/db", env!("CARGO_MANIFEST_DIR"));
        let mut cfg = Config::new().unwrap();
        cfg.db_path = db_location;

        let alpm = alpm_with_conf(&cfg).unwrap();
        let deps = get_package_dependencies(&alpm, "firefox");
        assert!(deps.is_ok());
        let deps = deps.unwrap();
        assert!(!deps.is_empty());
        let gtk3 = deps.first().unwrap();
        assert_eq!(gtk3.name(), "gtk3");

        let deps = get_package_dependencies(&alpm, "go");
        assert!(deps.is_ok());
        assert!(deps.unwrap().is_empty());

        let deps = get_package_dependencies(&alpm, "i_do_not_exists");
        assert_eq!(
            deps.err().unwrap().to_string(),
            Error::PackageNotFound("i_do_not_exists").to_string()
        );
    }

    #[test]
    fn test_get_package_optional_dependencies() {
        let db_location = format!("{}/tests/db", env!("CARGO_MANIFEST_DIR"));
        let mut cfg = Config::new().unwrap();
        cfg.db_path = db_location;

        let alpm = alpm_with_conf(&cfg).unwrap();
        let deps = get_package_optional_dependencies(&alpm, "firefox");
        assert!(deps.is_ok());

        let deps = deps.unwrap();
        assert!(!deps.is_empty());

        let nm = deps.first().unwrap();
        assert_eq!(nm.name(), "networkmanager");

        let deps = get_package_optional_dependencies(&alpm, "artix-mirrorlist");
        assert!(deps.is_ok());
        assert!(deps.unwrap().is_empty());

        let deps = get_package_optional_dependencies(&alpm, "i_do_not_exists");
        assert_eq!(
            deps.err().unwrap().to_string(),
            Error::PackageNotFound("i_do_not_exists").to_string()
        );
    }

    #[test]
    fn test_get_package_version() {
        let db_location = format!("{}/tests/db", env!("CARGO_MANIFEST_DIR"));
        let mut cfg = Config::new().unwrap();
        cfg.db_path = db_location;

        let alpm = alpm_with_conf(&cfg).unwrap();
        let ver = get_package_version(&alpm, "firefox");
        assert!(ver.is_ok());

        let ver = ver.unwrap();
        assert!(!ver.is_empty());

        let deps = get_package_version(&alpm, "i_do_not_exists");
        assert_eq!(
            deps.err().unwrap().to_string(),
            Error::PackageNotFound("i_do_not_exists").to_string()
        );
    }

    #[test]
    fn test_get_package() {
        let db_location = format!("{}/tests/db", env!("CARGO_MANIFEST_DIR"));
        let mut cfg = Config::new().unwrap();
        cfg.db_path = db_location;

        let alpm = alpm_with_conf(&cfg).unwrap();
        let pkg = get_package(&alpm, "artix-mirrorlist");
        assert!(pkg.is_ok());

        let pkg = pkg.unwrap();
        assert_eq!(pkg.name(), "artix-mirrorlist");
        assert_eq!(pkg.desc(), Some("Artix mirror list for use by pacman"));
        assert_eq!(pkg.arch(), Some("any"));
        assert!(!pkg.licenses().is_empty());
        assert_eq!(pkg.licenses().first(), Some("GPL"));
        assert_eq!(pkg.url(), Some("https://gitea.artixlinux.org/packagesA/artix-mirrorlist/src/branch/master/trunk/mirrorlist"));
        assert!(pkg.groups().is_empty());
        let dep = pkg.provides().first().unwrap();
        assert_eq!(dep.name(), "pacman-mirrorlist");
        let dep = pkg.conflicts().first().unwrap();
        assert_eq!(dep.name(), "pacman-mirrorlist");
        let dep = pkg.provides().first().unwrap();
        assert_eq!(dep.name(), "pacman-mirrorlist");
        assert_eq!(
            pkg.packager(),
            Some("Artix Build Bot <jenkins@artixlinux.org>")
        );
    }
}
