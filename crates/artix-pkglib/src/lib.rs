/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

//! This crate provides libalpm functionality for Artix Web Packages

#![deny(
    nonstandard_style,
    rust_2018_idioms,
    rust_2021_compatibility,
    missing_docs
)]
#![warn(clippy::all, clippy::pedantic)]

mod actions;
mod error;

pub mod prelude {
    //! The `artix_pkglib` prelude.
    //!
    //! This module alleviates import paths of many common imports
    //!
    //! ```
    //! # #![allow(unused_imports)]
    //! use artix_pkglib::prelude::*;
    //! ```

    pub use crate::actions::{
        database::{get_all_packages, get_packages, PackagesResultData},
        package::{
            get_package, get_package_dependencies, get_package_optional_dependencies,
            get_package_version, retrieve_providers,
        },
    };
    pub use crate::error::Error as PkglibError;

    // re-export part al alpm, alpm_utils and pacmanconf
    pub use alpm::{Alpm, AlpmList, Dep, DepMod, Package, SigLevel};
    pub use alpm_utils::alpm_with_conf;
    pub use pacmanconf::{Config, Repository};
}
