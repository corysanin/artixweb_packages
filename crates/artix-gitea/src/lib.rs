/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

//! This crate provides gitea functionality for Artix Web Packages

#![deny(
    nonstandard_style,
    rust_2018_idioms,
    rust_2021_compatibility,
    missing_docs
)]
#![warn(clippy::all, clippy::pedantic)]

mod client;

pub mod prelude {
    //! The `artix_gitea` prelude.
    //!
    //! This module alleviates imports paths of many common imports
    //!
    //! ```
    //! # #![allow(unused_imports)]
    //! use artix_gitea::prelude::*;
    //! ```

    pub use crate::client::{
        get_organization_ids, get_organization_repositories, Repository, GITEA_API_URL, GITEA_URL,
    };
}
