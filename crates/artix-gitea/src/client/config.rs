/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

pub const USER_AGENT: &str = "artixweb-packages";

/// Defines the default Artix Gitea URL
pub const GITEA_URL: &str = "https://gitea.artixlinux.org";
/// Defines the default Artix Gitea API URL
pub const GITEA_API_URL: &str = "https://gitea.artixlinux.org/api/v1";

// version as defined in the cargo for the package
pub static VERSION: &str = env!("CARGO_PKG_VERSION");
