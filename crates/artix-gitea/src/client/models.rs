/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

use awc::Client;
use chrono::{DateTime, Utc};
use serde::Deserialize;

use super::config;

// Defines an (partial) Organization in Gitea API
#[derive(Debug, Deserialize)]
pub struct Org {
    username: String,
}

impl Org {
    /// Retruns back this `Org` `username`
    pub fn username(&self) -> String {
        self.username.clone()
    }
}

/// Defines a (partial) Repository in Gitea API
#[derive(Debug, Deserialize)]
pub struct Repository {
    /// The repository numerical ID
    id: i32,

    /// The repository name
    name: String,

    /// The repository full name (organization name)
    full_name: String,

    /// The repository default branch
    default_branch: String,

    /// The repository clone URL
    clone_url: String,

    /// The repository HTML URL
    html_url: String,

    /// Last time that the repository was updated
    updated_at: DateTime<Utc>,
}

impl Repository {
    /// Tries to retrieve the specified repository from Artix Gitea
    ///
    /// If there is an error reaching the repository, returns `None`
    pub async fn retrieve_repository(
        organization: &str,
        package_name: &str,
        gitea_url: Option<&str>,
    ) -> Option<Repository> {
        let client = Client::default();

        let response = client
            .get(format!(
                "{}/repos/{}/{}",
                gitea_url.unwrap_or(config::GITEA_API_URL),
                organization,
                package_name
            ))
            .insert_header((
                "User-Agent",
                format!("{}/{}", config::USER_AGENT, config::VERSION),
            ))
            .send()
            .await;

        if let Ok(mut res) = response {
            if res.status().is_success() {
                return match res.json::<Repository>().await {
                    Ok(data) => Some(data),
                    Err(err) => {
                        println!("error: {:?}", err);
                        None
                    }
                };
            }
        }

        None
    }

    /// Retrieves this `Repository` PKGBUILD raw data
    ///
    /// If there is an error reaching the file, returns `None`
    pub async fn retrieve_pkgbuild_file(&self, gitea_url: Option<&str>) -> Option<String> {
        let client = Client::default();

        // Universe repositories does not have a trunk directory
        let pkg = if self.full_name.contains("Universe") {
            format!(
                "{}/raw/branch/{}/PKGBUILD",
                self.full_name, self.default_branch
            )
        } else {
            format!(
                "{}/raw/branch/{}/trunk/PKGBUILD",
                self.full_name, self.default_branch
            )
        };

        let response = client
            .get(format!(
                "{}/{}",
                gitea_url.unwrap_or(config::GITEA_URL),
                pkg
            ))
            .insert_header((
                "User-Agent",
                format!("{}/{}", config::USER_AGENT, config::VERSION),
            ))
            .send()
            .await;

        if let Ok(mut res) = response {
            if res.status().is_success() {
                if let Ok(data) = res.body().await {
                    return Some(
                        String::from_utf8(data.to_vec()).expect("An UTF-8 formatted string"),
                    );
                }
            }
        }

        None
    }

    /// Returns this `Repository` unique identifier as an i32 value
    #[must_use]
    pub fn id(&self) -> i32 {
        self.id
    }

    /// Returns this `Repository` `name` as a String value
    #[must_use]
    pub fn name(&self) -> String {
        self.name.clone()
    }

    /// Returns this `Repository` `full_name` as a String value
    #[must_use]
    pub fn full_name(&self) -> String {
        self.full_name.clone()
    }

    /// Returns this `Repository` `clone_url` as a String value
    #[must_use]
    pub fn clone_url(&self) -> String {
        self.clone_url.clone()
    }

    /// Returns this `Repository` `html_url` as a String value
    #[must_use]
    pub fn html_url(&self) -> String {
        self.html_url.clone()
    }

    /// Returns this `Repository` `updated_at` as a `DateTime<Utc>` value
    #[must_use]
    pub fn updated_at(&self) -> DateTime<Utc> {
        self.updated_at
    }
}

/*
   Module Tests
*/
#[cfg(test)]
mod test {
    use super::super::get_organization_repositories;
    use super::Repository;

    #[actix_web::test]
    async fn test_retrieve_repository() {
        let repo = Repository::retrieve_repository("packagesL", "linux", None).await;
        assert!(repo.is_some());
        let repo = repo.unwrap();
        assert_eq!(repo.name(), "linux");
    }

    #[actix_web::test]
    async fn test_repository() {
        let repositories = get_organization_repositories("Universe", None)
            .await
            .expect("Expect to get Universe repository data from Artix Gitea API");

        assert!(!repositories.is_empty());
        let repository = &repositories[0];
        let pkgbuild = repository
            .retrieve_pkgbuild_file(None)
            .await
            .unwrap_or_else(|| panic!("Expected a PKGBUILD file for {}", repository.full_name()));

        assert!(!pkgbuild.is_empty());
    }
}
