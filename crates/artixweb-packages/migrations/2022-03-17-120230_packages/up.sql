-- Table 'packages' is used to store local information about a given package
CREATE TABLE packages (
    package_name VARCHAR NOT NULL UNIQUE PRIMARY KEY,   -- This actually also includes the package version
    gitea_url VARCHAR NOT NULL,                         -- Gitea repository URL
    last_update TIMESTAMP NOT NULL,                              -- Last time this package was updated in gitea
)