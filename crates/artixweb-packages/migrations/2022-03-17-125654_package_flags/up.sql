-- Table 'package_flags' is a relation table used by users to flag outdated packages
CREATE TABLE package_flags (
    user_email VARCHAR(100) NOT NULL,
    package_name VARCHAR NOT NULL,
    flag_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (user_email, package_name),

    -- trolling flags?, no issue, just delete the user
    CONSTRAINT fk_user_email
        FOREIGN KEY (user_email) REFERENCES users (email) ON DELETE CASCADE,

    -- package unmaintained?, no issue, just delete the package
    CONSTRAINT fk_package_id
        FOREIGN KEY (package_name) REFERENCES packages (package_name) ON DELETE CASCADE
)