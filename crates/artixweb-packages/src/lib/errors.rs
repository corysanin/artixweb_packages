/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

use actix_web::{
    error,
    http::{header::ContentType, StatusCode},
    HttpResponse,
};
use askama::Template;
use derive_more::Display;
use diesel::result::{DatabaseErrorKind, Error as DBError};
use uuid::Error as ParseError;

#[derive(Debug, Display)]
pub enum ArtixWebPackageError {
    #[display(fmt = "not found")]
    NotFound,

    #[display(fmt = "could not acquire alpm handler: {}", _0)]
    NoAlpmHandler(String),
}

impl error::ResponseError for ArtixWebPackageError {
    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code())
            .insert_header(ContentType::html())
            .body(self.to_string())
    }

    fn status_code(&self) -> StatusCode {
        match *self {
            Self::NotFound => StatusCode::NOT_FOUND,
            Self::NoAlpmHandler(_) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

#[derive(Debug, Display)]
pub enum ServiceError {
    #[display(fmt = "Internal Server Error")]
    InternalServerError,

    #[display(fmt = "BadRequest: {}", _0)]
    BadRequest(String, Option<std::time::Instant>),

    #[display(fmt = "Unauthorized")]
    Unauthorized,
}

#[derive(Template)]
#[template(path = "error.html")]
struct AlreadyExistsTemplate {
    error_message: String,
    user_email: String,
    generation_time: u128,
}

#[derive(Template)]
#[template(path = "error.html")]
struct UnauthorizedTemplate {
    error_message: String,
    user_email: String,
    generation_time: u128,
}

impl error::ResponseError for ServiceError {
    fn error_response(&self) -> HttpResponse {
        match self {
            ServiceError::InternalServerError => HttpResponse::InternalServerError()
                .reason("Internal Server Error, Please try later")
                .body("Internal Server Error, Please try later"),
            ServiceError::BadRequest(ref message, start_time) => {
                let t = if let Some(start_time) = start_time {
                    start_time.elapsed().as_millis()
                } else {
                    0
                };

                HttpResponse::BadRequest().body(
                    AlreadyExistsTemplate {
                        user_email: String::new(),
                        error_message: message.clone(),
                        generation_time: t,
                    }
                    .render()
                    .unwrap(),
                )
            }
            ServiceError::Unauthorized => HttpResponse::Unauthorized().reason("Unauthorized").body(
                UnauthorizedTemplate {
                    user_email: String::new(),
                    error_message: String::from("You are not authorized to access this resource"),
                    generation_time: 0,
                }
                .render()
                .unwrap(),
            ),
        }
    }
}

impl From<ParseError> for ServiceError {
    fn from(_: ParseError) -> ServiceError {
        ServiceError::BadRequest("Invalid UUID".into(), None)
    }
}

impl From<DBError> for ServiceError {
    fn from(error: DBError) -> ServiceError {
        match error {
            DBError::DatabaseError(kind, info) => {
                if let DatabaseErrorKind::UniqueViolation = kind {
                    let mut message = info.details().unwrap_or_else(|| info.message()).to_string();
                    if message.contains("Key (email)=") {
                        message = format!("Can not register user, already exists: {}", message);
                    }
                    return ServiceError::BadRequest(message, None);
                }
                ServiceError::InternalServerError
            }
            _ => ServiceError::InternalServerError,
        }
    }
}
