/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

//! Packages route handlers definitions

#![allow(clippy::unused_async, clippy::module_name_repetitions)]

use std::fmt::Display;

use actix_web::{web, HttpResponse};
use artix_gitea::prelude::Repository;
use artix_pkglib::prelude::{
    get_package, retrieve_providers, Alpm, AlpmList, Dep, DepMod, Package,
};
use askama::Template;
use chrono::prelude::NaiveDateTime;
use diesel::prelude::*;
use diesel::PgConnection;
use lettre::{transport::smtp::authentication::Credentials, Message, SmtpTransport, Transport};
use serde::{Deserialize, Serialize};

use crate::config::SmtpOptions;
use crate::models::PackageWithFlagView;
use crate::models::{Package as PackageMeta, PrivatePackage};
use crate::{
    config::{sync_databases, SETTINGS},
    lib::{
        database::packages,
        errors::{ArtixWebPackageError, ServiceError},
        templates::PackageFlagEmail,
    },
    models::{PackageFlag, Pool},
};

use super::auth::LoggedUser;

#[derive(Serialize, PartialEq)]
pub(crate) enum DependencyKind {
    Make,
    Depend,
    Opt,
    Soname,
    Misc,
}

#[derive(Serialize)]
pub(crate) struct Dependency {
    pub kind: DependencyKind,
    pub name: String,
    pub ver: String,
    pub depmod: String,
    pub providers: Vec<String>,
}

impl Display for Dependency {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&format!(
            "{}{}{}",
            super::details_url(&self.name),
            if self.depmod.is_empty() {
                String::new()
            } else {
                format!(" {}", self.depmod)
            },
            if self.ver.is_empty() {
                String::new()
            } else {
                format!(" {}", self.ver)
            },
        ))
    }
}

#[derive(Serialize)]
pub(crate) struct Response {
    pub repository: String,
    pub package_name: String,
    pub version: String,
    pub description: String,
    pub last_update: NaiveDateTime,
    pub flag_on: Option<NaiveDateTime>,
    pub flagged: bool,
}

#[derive(Serialize)]
pub(crate) struct ResponseDetail {
    pub repo: String,
    pub package_name: String,
    pub version: String,
    pub architecture: String,
    pub description: String,
    pub upstream_url: String,
    pub licenses: Vec<String>,
    pub size: i64,
    pub installed_size: i64,
    pub build_date: i64,
    pub last_updated: i64,
    pub groups: Vec<String>,
    pub gitea_url: String,
    pub dependencies: Vec<Dependency>,
    pub sonames: Vec<Dependency>,
    pub makedepends: Vec<Dependency>,
    pub optdepends: Vec<Dependency>,
    pub provides: Vec<Dependency>,
    pub replaces: Vec<Dependency>,
    pub required_by: Vec<String>,
    pub contents: Vec<String>,
    pub maintainers: Vec<String>,
    pub flagged: bool,
    pub flagged_on: i64,
    pub flagged_by: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct FlagData {
    pub message: String,
}

#[derive(Debug, Deserialize)]
pub struct PkgData {
    pub package_doublet: String,
}

#[derive(Template)]
#[template(path = "flag_package.html")]
struct FlagPackageTemplate {
    package_doublet: String,
    package_name: String,
    package_version: String,
    user_email: String,
    arch: String,
    repo: String,
    generation_time: u128,
}

#[derive(Template)]
#[template(path = "error.html")]
struct PackageAlreadyFlaggedTemplate {
    error_message: String,
    user_email: String,
    generation_time: u128,
}

#[derive(Template)]
#[template(path = "error.html")]
struct PackageNotFoundTemplate {
    error_message: String,
    user_email: String,
    generation_time: u128,
}

/// Flags the given package as outdated
///
/// # Notes
///
/// The URL path paremeter is: `package_name`
pub async fn flag_package(
    data: web::Path<(String, String)>,
    flag_data: web::Form<FlagData>,
    logged_user: LoggedUser,
    pool: web::Data<Pool>,
    smtp_cfg: web::Data<SmtpOptions>,
) -> Result<HttpResponse, actix_web::Error> {
    let start_time = std::time::Instant::now();
    let user_name = logged_user.email.clone();
    let package_doublet = format!("{}-{}", data.0, data.1);
    web::block(move || {
        flag_package_query(&format!("{}-{}", data.0, data.1), &logged_user.email, &pool)
    })
    .await??;

    let email = Message::builder()
        .from(
            "ArtixWeb Packages <artixweb@artixlinux.org>"
                .parse()
                .unwrap(),
        )
        .to("artix-dev@artixlinux.org".parse().unwrap())
        .subject(format!(
            "Package {} has been flagged as ot-of-date",
            package_doublet
        ))
        .body(
            PackageFlagEmail {
                package_name: package_doublet,
                flag_by: user_name,
                message: flag_data.message.clone(),
            }
            .render()
            .unwrap(),
        )
        .unwrap();

    let smtp_credentials = Credentials::new(smtp_cfg.user.clone(), smtp_cfg.password.clone());
    let mailer = if let Ok(transport) = SmtpTransport::starttls_relay(&smtp_cfg.relay.clone()) {
        transport.credentials(smtp_credentials).build()
    } else {
        // open a local connection on port 25
        SmtpTransport::unencrypted_localhost()
    };

    // send the email
    match mailer.send(&email) {
        Ok(_) => Ok(HttpResponse::Ok().finish()),
        Err(e) => {
            log::error!("{}", e);
            Err(crate::lib::errors::ServiceError::BadRequest(
                format!("could not send email: {:?}", e),
                Some(start_time),
            )
            .into())
        }
    }
}

/// delete the given package flags metadata
///
/// # Example Query
///
/// ```text
/// curl -X DELETE -H "X-Admin-Token: <token>"" https://packages.artixlinux.org/akonadi/21.12.3-2
/// ```
pub async fn flag_package_delete(
    req: actix_web::HttpRequest,
    data: web::Path<(String, String)>,
    pool: web::Data<Pool>,
) -> HttpResponse {
    let package_doublet = format!("{}-{}", data.0, data.1);
    if let Some(token) = req.headers().get("x-admin-token") {
        if token != &SETTINGS.api_token {
            return HttpResponse::Unauthorized().finish();
        }
    }

    if let Ok(result) = web::block(move || flag_package_delete_query(&package_doublet, &pool)).await
    {
        match result {
            Ok(_) => HttpResponse::Ok().finish(),
            Err(_) => HttpResponse::NotFound().finish(),
        }
    } else {
        HttpResponse::InternalServerError().finish()
    }
}

// makes the query to delete he flag package from the database
fn flag_package_delete_query(
    package_doublet: &str,
    pool: &web::Data<Pool>,
) -> Result<(), crate::lib::errors::ServiceError> {
    use crate::schema::package_flags::dsl::{package_flags, package_name};

    let conn: &PgConnection = &pool.get().unwrap();
    diesel::delete(package_flags.filter(package_name.eq(package_doublet))).execute(conn)?;
    Ok(())
}

// create a new PackageFlag instance and insert it into the database
fn flag_package_query(
    package_doublet: &str,
    email: &str,
    pool: &web::Data<Pool>,
) -> Result<(), crate::lib::errors::ServiceError> {
    use crate::schema::package_flags::dsl::package_flags;

    let pf = PackageFlag::from_details(email, package_doublet);
    let conn: &PgConnection = &pool.get().unwrap();
    diesel::insert_into(package_flags)
        .values(&pf)
        .execute(conn)?;

    Ok(())
}

/// Present a form for registered users to flag a package
///
/// # Errors
///
/// If the package can not be found returns a 404 error
pub async fn flags_package_ui(
    data: web::Path<(String, String)>,
    logged_user: LoggedUser,
    pool: web::Data<Pool>,
) -> Result<HttpResponse, actix_web::Error> {
    // let package_name = package_name.into_inner();
    let start_time = std::time::Instant::now();
    let package_name = data.0.clone();
    let package_version = data.1.clone();
    let package_doublet = format!("{}-{}", package_name, package_version);

    // check if this package has been already flagged by the user
    let pkgname = data.0.clone();
    let pkgver = data.1.clone();
    let user_email = logged_user.email.clone();
    let result = web::block(move || {
        get_package_flags_query(
            &format!("{}-{}", pkgname, pkgver),
            &user_email,
            &pool,
            start_time,
        )
    })
    .await??;

    if result {
        // the user has already flag this package_name (plus version)
        return Ok(HttpResponse::Ok().content_type("text/html").body(
            PackageAlreadyFlaggedTemplate {
                error_message: format!("You have already flag package {}", package_doublet),
                user_email: logged_user.email,
                generation_time: start_time.elapsed().as_millis(),
            }
            .render()
            .unwrap(),
        ));
    }

    let alpm = Alpm::new("/", &SETTINGS.databases_path);
    if let Ok(handle) = alpm {
        sync_databases(&handle);
        let pkg_data = get_package(&handle, &package_name);
        if let Ok(pkg) = pkg_data {
            return Ok(HttpResponse::Ok().content_type("text/html").body(
                FlagPackageTemplate {
                    package_doublet,
                    package_version,
                    package_name: data.0.clone(),
                    user_email: logged_user.email,
                    arch: if let Some(arch) = pkg.arch() {
                        String::from(arch)
                    } else {
                        String::from("unknown")
                    },
                    repo: if let Some(db) = pkg.db() {
                        String::from(db.name())
                    } else {
                        String::from("unknown")
                    },
                    generation_time: start_time.elapsed().as_millis(),
                }
                .render()
                .unwrap(),
            ));
        }
    }

    return Ok(HttpResponse::NotFound().content_type("text/html").body(
        PackageNotFoundTemplate {
            user_email: logged_user.email,
            error_message: format!(
                "The specified package {} could not be found in the Artix packages database.",
                package_name
            ),
            generation_time: start_time.elapsed().as_millis(),
        }
        .render()
        .unwrap(),
    ));
}

fn get_package_flags_query(
    pkg_id: &str,
    email: &str,
    pool: &web::Data<Pool>,
    start_time: std::time::Instant,
) -> Result<bool, crate::lib::errors::ServiceError> {
    use crate::schema::package_flags::dsl::{package_flags, package_name, user_email};

    let conn: &PgConnection = &pool.get().unwrap();
    package_flags
        .filter(package_name.eq_all(pkg_id))
        .filter(user_email.eq_all(email))
        .load::<PackageFlag>(conn)
        .map_err(|_db_error| {
            ServiceError::BadRequest(
                format!("Invalid Package {} and email {}", pkg_id, email),
                Some(start_time),
            )
        })
        .map(|mut result| {
            if result.pop().is_some() {
                return true;
            }
            false
        })
}

/// Get list of packages orered in descending alphabetic order
/// with the given limit and offset
///
/// # Notes
///
/// The URL path parameters are: repo, limit, osset
pub async fn get_packages<'a>(
    data: web::Path<(String, usize, usize)>,
) -> Result<HttpResponse, ArtixWebPackageError> {
    if let Ok(result) = get_packages_inner(&data.0, data.1, data.2, None, None).await {
        return Ok(HttpResponse::Ok()
            .content_type("application/json; charset=utf-8")
            .insert_header(("X-Packages-Number", result.0.len()))
            .insert_header(("X-Packages-Total", result.1))
            .json(web::Json(result.0)));
    }
    Err(ArtixWebPackageError::NotFound)
}

/// Get the given package details
pub async fn get_package_details(
    data: web::Path<String>,
    pool: web::Data<Pool>,
) -> Result<HttpResponse, ArtixWebPackageError> {
    match get_packages_details_inner(&data, pool).await {
        Ok(result) => Ok(HttpResponse::Ok()
            .content_type("application/json; charset=utf-8")
            .json(web::Json(result))),
        Err(err) => Err(err),
    }
}

#[allow(clippy::too_many_lines)] // tell the linter to chill out a bit
pub(crate) async fn get_packages_details_inner(
    package_name: &str,
    pool: web::Data<Pool>,
) -> Result<ResponseDetail, ArtixWebPackageError> {
    let alpm = Alpm::new("/", &SETTINGS.databases_path);
    if let Ok(handle) = alpm {
        sync_databases(&handle);
        let pkg_data = get_package(&handle, package_name);
        if let Ok(pkg) = pkg_data {
            // create an initial ResponseDetail instance with filled pkg information from alpm
            let mut result = ResponseDetail {
                repo: if let Some(db) = pkg.db() {
                    db.name().to_string()
                } else {
                    String::from("unknown")
                },
                package_name: pkg.name().to_string(),
                version: pkg.version().to_string(),
                architecture: pkg.arch().unwrap_or("").to_string(),
                description: pkg.desc().unwrap_or("").to_string(),
                upstream_url: pkg.url().unwrap_or("").to_string(),
                licenses: pkg
                    .licenses()
                    .iter()
                    .map(std::string::ToString::to_string)
                    .collect(),
                size: pkg.size(),
                installed_size: pkg.isize(),
                build_date: pkg.build_date(),
                last_updated: 0,
                groups: pkg
                    .groups()
                    .iter()
                    .map(std::string::ToString::to_string)
                    .collect(),
                gitea_url: String::new(),
                dependencies: get_depends_from_package(
                    &handle,
                    pkg.arch().unwrap_or("any"),
                    pkg.depends(),
                    &DependencyKind::Depend,
                ),
                sonames: get_depends_from_package(
                    &handle,
                    pkg.arch().unwrap_or("any"),
                    pkg.depends(),
                    &DependencyKind::Soname,
                ),
                makedepends: get_depends_from_package(
                    &handle,
                    pkg.arch().unwrap_or("any"),
                    pkg.makedepends(),
                    &DependencyKind::Make,
                ),
                optdepends: get_depends_from_package(
                    &handle,
                    pkg.arch().unwrap_or("any"),
                    pkg.optdepends(),
                    &DependencyKind::Opt,
                ),
                required_by: pkg
                    .required_by()
                    .iter()
                    .map(std::string::ToString::to_string)
                    .collect(),
                contents: pkg
                    .files()
                    .files()
                    .iter()
                    .map(|file| file.name().to_string())
                    .collect(),
                maintainers: Vec::new(),
                provides: get_depends_from_package(
                    &handle,
                    pkg.arch().unwrap_or("any"),
                    pkg.provides(),
                    &DependencyKind::Misc,
                ),
                replaces: get_depends_from_package(
                    &handle,
                    pkg.arch().unwrap_or("any"),
                    pkg.replaces(),
                    &DependencyKind::Misc,
                ),
                flagged: false,
                flagged_on: 0,
                flagged_by: Vec::new(),
            };

            let pkg_name_version = format!("{}-{}", pkg.name(), pkg.version());
            let p_pool = pool.clone();
            if let Ok(Ok(metadata)) =
                web::block(move || get_package_metadata(&pkg_name_version, &pool)).await
            {
                result.last_updated = metadata.last_update.timestamp();
                result.gitea_url = metadata.gitea_url;
                result.flagged = metadata.flagged;
                if let Some(flagged_on) = metadata.flag_on {
                    result.flagged_on = flagged_on.timestamp_millis();
                }
                result.flagged_by = metadata
                    .flaggers
                    .iter()
                    .filter(|f| f.is_some())
                    .map(|f| f.clone().unwrap())
                    .collect();
                return Ok(result);
            }

            // look for this package in gitea
            if let Some(gitea_repo) = Repository::retrieve_repository(
                &String::from("packages"),
                package_name,
                Some(&SETTINGS.gitea_api_url),
            )
            .await
            {
                result.last_updated = gitea_repo.updated_at().timestamp();
                result.gitea_url = gitea_repo.html_url();
                // result.maintainers = retrieve_maintainers(&gitea_repo).await;
            }
            // add package metadata to database (if we are here it means database has not been filled yet)
            let metadata = PackageMeta {
                package_name: format!("{}-{}", pkg.name(), pkg.version()),
                gitea_url: result.gitea_url.clone(),
                last_update: chrono::NaiveDateTime::from_timestamp(result.last_updated, 0),
            };
            if web::block(move || add_package_metadata(&metadata, &p_pool))
                .await
                .is_err()
            {
                dbg!("Some error occurred while storing package metadata at async level");
            }
            return Ok(result);
        }
    } else {
        return Err(ArtixWebPackageError::NoAlpmHandler(
            alpm.err().unwrap().to_string(),
        ));
    }

    Err(ArtixWebPackageError::NotFound)
}

// adds package metadata into the local postgres database
fn add_package_metadata(metadata: &PackageMeta, pool: &web::Data<Pool>) {
    use crate::schema::packages::dsl::packages;

    let conn: &PgConnection = &pool.get().unwrap();
    if let Err(err) = diesel::insert_into(packages).values(metadata).execute(conn) {
        dbg!(err.to_string());
    }
}

// retrieves package metadata from the local postgres database
fn get_package_metadata<'a>(
    pkg_doublet: &'a str,
    pool: &'a web::Data<Pool>,
) -> Result<PrivatePackage, crate::lib::errors::ServiceError> {
    use crate::schema::{package_flags, packages};

    let start_time = std::time::Instant::now();
    let conn: &PgConnection = &pool.get().unwrap();
    let metadata: Vec<(PackageMeta, Option<chrono::NaiveDateTime>, Option<String>)> =
        packages::table
            .find(pkg_doublet)
            .left_join(
                package_flags::table.on(package_flags::package_name
                    .eq(packages::package_name)
                    .and(package_flags::flag_on.ge(packages::last_update))),
            )
            .select((
                packages::all_columns,
                package_flags::flag_on.nullable(),
                package_flags::user_email.nullable(),
            ))
            .load(conn)
            .map_err(|_db_error| {
                ServiceError::BadRequest(
                    format!("Could not find package {}", pkg_doublet),
                    Some(start_time),
                )
            })?;

    if let Some(user_metadata) = metadata.first() {
        let flaggers = metadata.iter().map(|meta| meta.2.clone()).collect();
        Ok(PrivatePackage::from(
            &user_metadata.0,
            user_metadata.1,
            flaggers,
        ))
    } else {
        Err(ServiceError::InternalServerError)
    }
}

pub(crate) async fn get_packages_inner(
    repo_names: &str,
    limit: usize,
    offset: usize,
    keyword: Option<&str>,
    pool: Option<&web::Data<Pool>>,
) -> Result<(Vec<Response>, usize), ArtixWebPackageError> {
    let start_time = std::time::Instant::now();
    let alpm = Alpm::new("/", &SETTINGS.databases_path);

    if let Ok(handle) = alpm {
        sync_databases(&handle);
        let mut repos = Vec::new();
        let mut result = Vec::<Response>::new();
        let repos_param = repo_names;
        let repos = match repos_param.to_lowercase().as_str() {
            "" | "all" => None,
            _ => {
                let split_data = repos_param.split(':');
                repos.extend(split_data);
                Some(repos)
            }
        };

        let pkgs = packages(&handle, limit, offset, repos, keyword);
        if pkgs.is_err() {
            return Err(ArtixWebPackageError::NotFound);
        }
        let pkgs = pkgs.unwrap();

        let flagged_packages = if let Some(pool) = pool {
            if let Ok(flagged_packages) = get_flagged_packages(start_time, pool).await {
                flagged_packages
            } else {
                Vec::new()
            }
        } else {
            Vec::new()
        };

        for pkg in pkgs.packages() {
            let pkg_doublet = format!("{}-{}", pkg.name(), pkg.version());
            let (flagged, flag_on) = if let Some(flag_data) = flagged_packages
                .iter()
                .find(|p| p.package_name == pkg_doublet)
            {
                (true, flag_data.flag_on)
            } else {
                (false, None)
            };

            result.push(Response {
                repository: if pkg.db().is_none() {
                    String::from("unknown")
                } else {
                    String::from(pkg.db().unwrap().name())
                },
                package_name: pkg.name().to_string(),
                version: pkg.version().as_str().to_string(),
                last_update: NaiveDateTime::from_timestamp(pkg.build_date(), 0),
                description: String::from(pkg.desc().unwrap_or_default()),
                flag_on,
                flagged,
            });
        }

        if result.is_empty() {
            return Err(ArtixWebPackageError::NotFound);
        }

        return Ok((result, pkgs.total()));
    }
    Err(ArtixWebPackageError::NotFound)
}

// get all flagged packages from the postgres database
async fn get_flagged_packages(
    start_time: std::time::Instant,
    pool: &web::Data<Pool>,
) -> Result<Vec<PackageWithFlagView>, crate::lib::errors::ServiceError> {
    use crate::schema::package_flags::dsl::{
        flag_on, package_flags, package_name as package_flag_name,
    };
    use crate::schema::packages::dsl::{last_update, package_name, packages};

    let conn: &PgConnection = &pool.get().unwrap();
    let packages_data: Vec<(PackageMeta, Option<chrono::NaiveDateTime>)> = packages
        .inner_join(
            package_flags.on(flag_on
                .ge(last_update)
                .and(package_name.eq(package_flag_name))),
        )
        .select((packages::all_columns(), flag_on.nullable()))
        .load(conn)
        .map_err(|_db_error| {
            ServiceError::BadRequest(String::from("Could not find packages"), Some(start_time))
        })?;

    Ok(packages_data
        .iter()
        .map(|pkg_data| PackageWithFlagView::from(&pkg_data.0, pkg_data.1))
        .collect())
}

#[allow(dead_code)]
async fn retrieve_maintainers(gitea_repo: &Repository) -> Vec<String> {
    let mut maintainers = Vec::new();
    if let Some(pkgbuild) = gitea_repo
        .retrieve_pkgbuild_file(Some(&SETTINGS.gitea_url))
        .await
    {
        let lines = pkgbuild.lines();
        let mut maintainers_found = false;
        for line in lines {
            let lower = line.to_lowercase();
            if lower.contains("maintainer") || lower.contains("contributor") {
                let data: Vec<String> = line.split(':').map(|m| m.trim().to_string()).collect();
                if data.len() > 1 {
                    maintainers.push(data[1].clone());
                }
                if !maintainers_found {
                    maintainers_found = true;
                }
            } else if maintainers_found {
                break;
            }
        }
    };
    maintainers
}

fn get_depends_from_package<'a>(
    alpm: &'a Alpm,
    arch: &str,
    deps: AlpmList<'_, &Dep>,
    kind: &DependencyKind,
) -> Vec<Dependency> {
    match kind {
        DependencyKind::Depend | DependencyKind::Soname => deps
            .iter()
            .map(|dep| construct_dependency(arch, alpm, dep))
            .filter(|d| d.kind == *kind)
            .collect(),
        DependencyKind::Make => deps
            .iter()
            .map(|dep| construct_make_dependency(arch, alpm, dep))
            .collect(),
        DependencyKind::Opt => deps
            .iter()
            .map(|dep| construct_opt_dependency(arch, alpm, dep))
            .collect(),
        DependencyKind::Misc => deps
            .iter()
            .map(|dep| construct_dependency(arch, alpm, dep))
            .collect(),
    }
}

// constructs a Dependency instance with the given data and returns it back
#[allow(clippy::needless_pass_by_value)]
fn construct_dependency(arch: &str, alpm: &Alpm, dep: &Dep) -> Dependency {
    let kind = if dep
        .name()
        .rsplit('.')
        .next()
        .map(|ext| ext.eq_ignore_ascii_case("so"))
        == Some(true)
    {
        DependencyKind::Soname
    } else {
        DependencyKind::Depend
    };

    common_dependency_data(arch, alpm, dep, kind)
}

// constructs a (make) Dependency instance with the given data and return it back
#[allow(clippy::needless_pass_by_value)]
fn construct_make_dependency(arch: &str, alpm: &Alpm, dep: &Dep) -> Dependency {
    common_dependency_data(arch, alpm, dep, DependencyKind::Make)
}

// constructs a (opt) Dependency instance with the given data and return it back
#[allow(clippy::needless_pass_by_value)]
fn construct_opt_dependency(arch: &str, alpm: &Alpm, dep: &Dep) -> Dependency {
    common_dependency_data(arch, alpm, dep, DependencyKind::Opt)
}

fn common_dependency_data<'a>(
    arch: &str,
    alpm: &'a Alpm,
    dep: &Dep,
    kind: DependencyKind,
) -> Dependency {
    let ver = if let Some(ver) = dep.version() {
        ver.to_string()
    } else {
        String::new()
    };

    let depmod = match dep.depmod() {
        DepMod::Any => String::new(),
        DepMod::Eq => String::from("=="),
        DepMod::Ge => String::from(">="),
        DepMod::Gt => String::from(">"),
        DepMod::Le => String::from("<="),
        DepMod::Lt => String::from("<"),
    };

    Dependency {
        name: dep.name().to_string(),
        providers: retrieve_providers(arch, alpm, dep.name())
            .iter()
            .map(|p| p.name().to_string())
            .collect(),
        kind,
        ver,
        depmod,
    }
}
