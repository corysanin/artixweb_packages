/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

//! Define all the application handlers

pub mod auth;
pub mod details;
pub mod index;
pub mod invitation;
pub mod packages;
pub mod register;

fn details_url(pkg_name: &str) -> String {
    let url = format!("/details/{}", pkg_name);
    url
}
