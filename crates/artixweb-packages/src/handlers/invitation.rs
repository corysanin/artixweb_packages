/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

use actix_web::{web, HttpResponse};
use askama::Template;
use diesel::{prelude::*, PgConnection};
use lettre::{transport::smtp::authentication::Credentials, Message, SmtpTransport, Transport};
use serde::Deserialize;

use crate::{
    config::{SmtpOptions, SETTINGS},
    lib::templates::EmailTemplate,
    models::{Invitation, Pool},
};

#[derive(Deserialize)]
pub struct Data {
    pub email: String,
}

pub async fn post(
    req: actix_web::HttpRequest,
    invitation_data: web::Json<Data>,
    pool: web::Data<Pool>,
    smtp_cfg: web::Data<SmtpOptions>,
) -> Result<HttpResponse, actix_web::Error> {
    if let Some(token) = req.headers().get("x-admin-token") {
        if token == &SETTINGS.api_token {
            web::block(move || {
                create_invitation(invitation_data.into_inner().email, &pool, &smtp_cfg)
            })
            .await??;
            Ok(HttpResponse::Ok().finish())
        } else {
            Ok(HttpResponse::Unauthorized().finish())
        }
    } else {
        Ok(HttpResponse::Unauthorized().finish())
    }
}

fn create_invitation(
    eml: String,
    pool: &web::Data<Pool>,
    smtp_cfg: &web::Data<SmtpOptions>,
) -> Result<(), crate::lib::errors::ServiceError> {
    let invitation = query(eml, pool)?;
    if let Err(err) = send_invitation(&invitation, smtp_cfg) {
        dbg!(err);
        println!("{:#?}", invitation);
    };
    Ok(())
}

fn send_invitation(
    invitation: &Invitation,
    smtp_cfg: &web::Data<SmtpOptions>,
) -> Result<(), crate::lib::errors::ServiceError> {
    let email = Message::builder()
        .from(
            "ArtixWeb Packages <artixweb@artixlinux.org>"
                .parse()
                .unwrap(),
        )
        .to(invitation.email.parse().unwrap())
        .subject("You have been invited to participate in ArtixWeb Packages as a reporter")
        .body(EmailTemplate { invitation }.render().unwrap())
        .unwrap();

    let smtp_credentials = Credentials::new(smtp_cfg.user.clone(), smtp_cfg.password.clone());
    let mailer = if let Ok(transport) = SmtpTransport::starttls_relay(&smtp_cfg.relay.clone()) {
        transport.credentials(smtp_credentials).build()
    } else {
        // open a local connection on port 25
        SmtpTransport::unencrypted_localhost()
    };

    // send the email
    match mailer.send(&email) {
        Ok(_) => Ok(()),
        Err(e) => {
            log::error!("{}", e);
            Err(crate::lib::errors::ServiceError::InternalServerError)
        }
    }
}

// Diesel query
fn query(
    eml: String,
    pool: &web::Data<Pool>,
) -> Result<Invitation, crate::lib::errors::ServiceError> {
    use crate::schema::invitations::dsl::invitations;

    let new_invitation: Invitation = eml.into();
    let conn: &PgConnection = &pool.get().unwrap();

    let inserted_invitation = diesel::insert_into(invitations)
        .values(&new_invitation)
        .get_result(conn)?;

    Ok(inserted_invitation)
}
