/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

#![allow(
    clippy::unused_async,
    clippy::module_name_repetitions,
    clippy::cast_precision_loss
)]

use actix_session::Session;
use actix_web::{web, HttpResponse, Result};
use askama::Template;

use crate::models::Pool;

use super::packages::{get_packages_inner, Response};

#[derive(Template)]
#[template(path = "base_index.html")]
struct BaseTemplate {
    packages: Vec<Response>,
    repos: Vec<String>,
    limit: usize,
    query: String,
    user_email: String,
    generation_time: u128,
}

#[derive(Template)]
#[template(path = "packages_nav.html")]
struct PackagesNavigation {
    packages: Vec<Response>,
    repos: Vec<String>,
    total: usize,
    offset: usize,
    limit: usize,
    query: String,
    user_email: String,
    total_pages: usize,
    generation_time: u128,
}

/// Constructs the index for the web site
#[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
pub async fn index(
    query: web::Query<Vec<(String, String)>>,
    session: Session,
    pool: web::Data<Pool>,
) -> Result<HttpResponse> {
    let start_time = std::time::Instant::now();
    let mut offset: usize = 0;
    let mut limit: usize = 50;
    let mut repos = Vec::new();
    let mut search_criteria: Option<&str> = None;
    let mut keyword: String = String::new();
    let mut query_url = Vec::new();
    let user_email = if let Some(email) = session.get::<String>("user_email").unwrap() {
        email
    } else {
        String::new()
    };

    let valid_repos = vec![
        "system",
        "world",
        "galaxy",
        "lib32",
        "system-gremlins",
        "world-gremlins",
        "galaxy-gremlins",
        "lib32-gremlins"
    ];
    for parameter in query.0 {
        let key: &str = &parameter.0;
        match key {
            "repo" => {
                let repo_name = parameter.1.to_lowercase();
                if valid_repos.contains(&repo_name.clone().as_ref()) {
                    repos.push(repo_name.clone());
                    query_url.push(format!("repo={}", repo_name));
                }
            }
            "search_criteria" => {
                keyword = parameter.1.clone();
                query_url.push(format!("search_criteria={}", parameter.1.clone()));
            }
            "page" => {
                offset = if let Ok(page) = parameter.1.parse::<usize>() {
                    page
                } else {
                    0
                }
            }
            "limit" => {
                limit = match parameter.1.as_str() {
                    "all" | "0" => 0,
                    _ => {
                        if let Ok(value) = parameter.1.parse::<usize>() {
                            value
                        } else {
                            50
                        }
                    }
                }
            }
            _ => {}
        }
    }

    if !keyword.is_empty() {
        search_criteria = Some(&keyword);
    }

    let repos_criteria = if repos.is_empty() {
        String::from("all")
    } else {
        repos.join(":")
    };

    let s = if let Ok(result) =
        get_packages_inner(&repos_criteria, limit, offset, search_criteria, Some(&pool)).await
    {
        PackagesNavigation {
            user_email,
            packages: result.0,
            repos,
            total: result.1,
            query: query_url.join("&"),
            total_pages: if limit > 0 {
                (result.1 as f64 / limit as f64).ceil() as usize
            } else {
                1
            },
            generation_time: start_time.elapsed().as_millis(),
            offset,
            limit,
        }
        .render()
        .unwrap()
    } else {
        return Ok(HttpResponse::NotFound()
            .content_type("text/html")
            .body("<html><body>404 - Page Not Found!</body></html>"));
    };

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[allow(clippy::unnecessary_wraps)]
mod filters {
    pub fn selected_repo(repos: &Vec<String>, repo: &str) -> ::askama::Result<bool> {
        Ok(repos.iter().any(|r| r == repo))
    }

    pub fn details_url(pkg_name: &str) -> ::askama::Result<String> {
        Ok(super::super::details_url(pkg_name))
    }
}
