/*
    This file is part of Artix Web Packages. See LICENSE file for details

    Copyright (c) 2022 - Artix Linux
    Copyright (c) 2022 - Oscar Campos <damnwidget@artixlinux.org>
*/

use diesel::{r2d2::ConnectionManager, PgConnection};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use super::schema::{invitations, package_flags, packages, users};

// type alias to use in multiple places
pub type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "packages"]
pub struct Package {
    pub package_name: String,
    pub gitea_url: String,
    pub last_update: chrono::NaiveDateTime,
}

impl Package {
    pub fn from_details<S: Into<String>, T: Into<String>>(name: S, url: T) -> Self {
        Package {
            package_name: name.into(),
            gitea_url: url.into(),
            last_update: chrono::Local::now().naive_local(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PrivatePackage {
    pub package_name: String,
    pub gitea_url: String,
    pub last_update: chrono::NaiveDateTime,
    pub flagged: bool,
    pub flag_on: Option<chrono::NaiveDateTime>,
    pub flaggers: Vec<Option<String>>,
}

impl PrivatePackage {
    pub fn from(
        pkg: &Package,
        flag_on: Option<chrono::NaiveDateTime>,
        flaggers: Vec<Option<String>>,
    ) -> Self {
        PrivatePackage {
            package_name: pkg.package_name.clone(),
            gitea_url: pkg.gitea_url.clone(),
            last_update: pkg.last_update,
            flagged: flag_on.is_some(),
            flag_on,
            flaggers,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PackageWithFlagView {
    pub package_name: String,
    pub gitea_url: String,
    pub last_update: chrono::NaiveDateTime,
    pub flag_on: Option<chrono::NaiveDateTime>,
}

impl PackageWithFlagView {
    pub fn from(pkg: &Package, flag_on: Option<chrono::NaiveDateTime>) -> Self {
        PackageWithFlagView {
            package_name: pkg.package_name.clone(),
            gitea_url: pkg.gitea_url.clone(),
            last_update: pkg.last_update,
            flag_on,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "users"]
pub struct User {
    pub email: String,
    pub hash: String,
    pub created_at: chrono::NaiveDateTime,
    pub banned: bool,
}

impl User {
    pub fn from_details<S: Into<String>, T: Into<String>>(email: S, pwd: T) -> Self {
        User {
            email: email.into(),
            hash: pwd.into(),
            created_at: chrono::Local::now().naive_local(),
            banned: false,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "invitations"]
pub(crate) struct Invitation {
    pub id: Uuid,
    pub email: String,
    pub expires_at: chrono::NaiveDateTime,
}

// any type that implements Into<String> can be used to create an invitation
impl<T> From<T> for Invitation
where
    T: Into<String>,
{
    fn from(email: T) -> Self {
        Invitation {
            id: uuid::Uuid::new_v4(),
            email: email.into(),
            expires_at: chrono::Local::now().naive_local() + chrono::Duration::hours(24),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "package_flags"]
pub struct PackageFlag {
    pub user_email: String,
    pub package_name: String,
    flag_on: chrono::NaiveDateTime,
}

impl PackageFlag {
    pub fn from_details<S: Into<String>, T: Into<String>>(user_email: S, package_name: T) -> Self {
        PackageFlag {
            user_email: user_email.into(),
            package_name: package_name.into(),
            flag_on: chrono::Local::now().naive_local(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SlimUser {
    pub email: String,
}

impl From<User> for SlimUser {
    fn from(user: User) -> Self {
        SlimUser { email: user.email }
    }
}
