-- ArtixWeb Databse Schema SQL

-- \connect "artixweb";

DROP TABLE IF EXISTS "__diesel_schema_migrations";
CREATE TABLE "public"."__diesel_schema_migrations" (
    "version" character varying(50) NOT NULL,
    "run_on" timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT "__diesel_schema_migrations_pkey" PRIMARY KEY ("version")
) WITH (oids = false);


DROP TABLE IF EXISTS "invitations";
CREATE TABLE "public"."invitations" (
    "id" uuid NOT NULL,
    "email" character varying(100) NOT NULL,
    "expires_at" timestamp NOT NULL,
    CONSTRAINT "invitations_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "package_flags";
CREATE TABLE "public"."package_flags" (
    "user_email" character varying(100) NOT NULL,
    "package_name" character varying NOT NULL,
    "flag_on" timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT "package_flags_pkey" PRIMARY KEY ("user_email", "package_name")
) WITH (oids = false);


DROP TABLE IF EXISTS "packages";
CREATE TABLE "public"."packages" (
    "package_name" character varying NOT NULL,
    "gitea_url" character varying NOT NULL,
    "last_update" timestamp NOT NULL,
    CONSTRAINT "packages_pkey" PRIMARY KEY ("package_name")
) WITH (oids = false);


DROP TABLE IF EXISTS "users";
CREATE TABLE "public"."users" (
    "email" character varying(100) NOT NULL,
    "hash" character varying(122) NOT NULL,
    "created_at" timestamp NOT NULL,
    "banned" boolean DEFAULT false NOT NULL,
    "admin" boolean DEFAULT false NOT NULL,
    CONSTRAINT "users_pkey" PRIMARY KEY ("email")
) WITH (oids = false);


ALTER TABLE ONLY "public"."package_flags" ADD CONSTRAINT "fk_package_id" FOREIGN KEY (package_name) REFERENCES packages(package_name) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."package_flags" ADD CONSTRAINT "fk_user_email" FOREIGN KEY (user_email) REFERENCES users(email) ON DELETE CASCADE NOT DEFERRABLE;
